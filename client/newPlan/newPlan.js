Template.newPlan.helpers({
    modules: function () {
        return ModuleList.find({});
    },
    breakLine: function () {
        return "<br>";
    },

});
Template.newPlan.events({
    'submit form'(event) {
        // Prevent default browser form submit
        event.preventDefault();

        // Get value from form element
        var target = event.target;
        var month = target.date.value;
        var day = target.day.value;
        var year = target.year.value;
        var hour = target.hour.value;
        var minute = target.minute.value;

        Session.set("month", month);
        Session.set("day", day);
        Session.set("year", year);
        Session.set("hour", hour);
        Session.set("minute", minute);
        // var displayModules = target.displayModules.value;
        //https://jsfiddle.net/j08691/pQD8g/
        var select1 = document.getElementById("displayModules");
        var selected1 = [];
        for (var i = 0; i < select1.length; i++) {
            if (select1.options[i].selected) selected1.push(select1.options[i].value);
        }

        Session.set("moduleIDs", selected1.toString());
        Router.go('/newPlan/chooseVenue/');


        // Clear form
        //target.text.value = '';
    },
});

window.onmousedown = function (e) {
    var el = e.target;
    if (el.tagName.toLowerCase() == 'option' && el.parentNode.hasAttribute('multiple')) {
        e.preventDefault();

        // toggle selection
        if (el.hasAttribute('selected')) el.removeAttribute('selected');
        else el.setAttribute('selected', '');

        // hack to correct buggy behavior
        var select = el.parentNode.cloneNode(true);
        el.parentNode.parentNode.replaceChild(select, el.parentNode);
    }
}