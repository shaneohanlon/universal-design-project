Template.loginButtons.rendered = function () {
    Accounts._loginButtonsSession.set('dropdownVisible', true);
    // $(".login-close-text").addClass("login-front");
//     Tracker.afterFlush( function () {
//     	this.$(".accounts-dialog").css("box-shadow","none");
//     this.$(".login-close-text").hide();
// }
}
Template.login.helpers({
    removeStyle: function () {
        console.log(document.getElementsByClassName(".accounts-dialog"));
        $(".accounts-dialog").css("box-shadow", "none");
        $(".login-close-text").hide();
    },
});

if (Meteor.isClient) {
    Meteor.startup(function () {
        var endtime = 'December 25 2015 18:50:30 UTC-0400';
        timeinterval = Meteor.setInterval(function () {
            Meteor.call("getCurrentTime", function (error, result) {
                Session.set("time", result);
                var t = getTimeRemaining(endtime);
                Session.set("t", t);
            });
        }, 1000);

        WebFontConfig = {
            google: {families: ['Archivo+Narrow', '   Indie Flower', 'Fira Mono']}
        };
        (function () {
            var wf = document.createElement('script');
            wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
                '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
            wf.type = 'text/javascript';
            wf.async = 'true';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(wf, s);
            console.log("async fonts loaded", WebFontConfig);
        })();
    });
}